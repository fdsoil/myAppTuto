<?php
//session_start();

//require("../../../" . $_SESSION ['FDSoil'] . "/packs/tcpdf/config/lang/eng.php");
//require("../../../" . $_SESSION ['FDSoil'] . "/packs/tcpdf/tcpdf.php");
use \FDSoil\TCPDF as TCPDF;

/*class MYPDF extends TCPDF
{
  public function Header(){
    $this->Image(K_PATH_IMAGES.'top_reporte.png', 25, 10, 170, 15);
    $this->Cell(30, 1, $this->title, 0, 0, 'C');
  }
}*/



/* creamos un documento con las dimensiones por defecto A4 Vertical*/
$pdf = new TCPDF( PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
/* desactivamos encabezados y pies de página por defecto */

$pdf->setPrintFooter(false);
$pdf->SetTitle('');
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('usuario');

$pdf->SetSubject('');
$pdf->SetKeywords('');
$pdf->setFont('times','',10);

$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, "", "");

$pdf->SetMargins(PDF_MARGIN_LEFT, 30, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
$pdf->setLanguageArray($l);
$pdf->AddPage();




//$obj->seeArray($_POST);
//$obj->seeArray($aRegMaest);

$remitente_cargo = "/ DIRECTORA GENERAL DEL DESPACHO";
$remitente_anexo = '<br/>'.'MPP DE ECONOMIA Y FINANZAS / DESPACHO DEL MINISTRO';
$remitente = "WENDY PINTO CASTILLO ";

// create some HTML content
// $html1 = '<h1 style="font-size:20>" style="text-align:center;"> <strong>NOTA ENTREGA N° '.$numero.'</strong></h1>';
$html1 = <<<EOD
<h1 style="font-size:20;text-align:center;"><strong>NOTA ENTREGA</strong></h1>
EOD;

$html = <<<EOD
<table width="100%" border="1" cellspacing="0" cellpadding="1" style="font-size:12;font-weight:bold;">
  <tr>
    <td style="font-size:15;text-align:center;"> Nº:    numero</td>
    <td style="font-size:15;text-align:center;"> Fecha: fecha_original</td>
  </tr>
</table>
EOD;

$htm2 = <<<EOD
<table width="100%" border="1" cellspacing="0" cellpadding="1" style="font-size:12;font-weight:bold;">
  <tr>
    <td>PARA</td>
    <td colspan="7">destinatario</td>
  </tr>
  <tr>
    <td>DE</td>
    <td colspan="7">jhjh</td>
  </tr>
</table>
EOD;

$htm3 = <<<EOD
<table width="100%" border="1" style="font-size:12;">
  <tr>
    <td><p style="font-size:20;text-align:center;font-weight:bold;">Asunto</p></td>
  </tr>
  <tr>
    <td style="height:480;max-height:590px;">
      <div>
        <p>asunto</p><br><br><br>
        <p style="text-align:center;font-weight:bold;margin-bottom:0px">mmm</p>
      </div>
    </td>
  </tr>
</table>
EOD;

// $html =
// "<table width=\"100%\" border=\"1\" cellspacing=\"0\"  style=\"font-size:12\"style=\"font-weight: bold \" cellpadding=\"0\">
// <tr>
// <td>
// <br>
// <P ALIGN=\"left\">&nbsp;PARA:&nbsp;&nbsp;&nbsp;$destinatario</P>
// <br>
// <br>
// <br> DE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WENDY PINTO<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
//  &nbsp;&nbsp;&nbsp;&nbsp;DIRECTORA GENERAL DEL DESPACHO  <br> &nbsp;&nbsp;&nbsp;&nbsp;
//  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; MINISTERIO DEL PODER POPULAR DE ECONOMIA Y FINANZAS <br>
//
// <br><br> ASUNTO: $asunto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; CORRELATIVO $correlativo<br>
// <br>
// <P ALIGN=\"RIGHT\">$fecha&nbsp;&nbsp;&nbsp;</P>
//  <br><br><br><br>
//
// </td>
//
// </tr>
//
//
// </table><br><br>
//
// ";

// output the HTML content
$pdf->writeHTML($html1, true, false, true, false, '');
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->writeHTML($htm2, true, false, true, false, '');
$pdf->writeHTML($htm3, true, false, true, false, '');



// reset pointer to the last page
$pdf->lastPage();



// $tabla = "<table width=\"100%\" border=\"1\" cellspacing=\"0\" style=\"font-weight: bold \" style=\"font-size:12\" cellpadding=\"0\">
// <tr>
// <td><br>
//   RECIBIDO POR:<br>
// <br>  NOMBRE Y APELLIDO:______________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;FECHA:_______________<br>
// <br><br>  HORA:__________________&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; TELÉFONO:__________________<br>
//
// &nbsp;&nbsp;&nbsp;
//
//
// </td></tr>
// <br><br>
// &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$creador_nota
//
// </table>";

$tabla = <<<EOD
  <table border="1" style="font-size:12;font-weight:bold;text-align:center;">
    <thead>
      <tr>
        <td> Recibido por </td>
        <td> Fecha </td>
        <td> Hora </td>
        <td> Firma </td>
        <td> Sello </td>
      </tr>
    <thead>
    <tbody>
      <tr>
         <td height="50"></td>
         <td></td>
         <td></td>
         <td></td>
         <td></td>
      </tr>
     </tbody>
  </table>
  <!--<table border="1" style="font-size:8;font-weight:bold; margin-left:0;" cellpadding="10">
    <tr>
      <td>arrNombres[0] arrApellidos[0]</td>
    </tr>
  </table>-->
  <p style="font-size:8;font-weight:bold;">arrNombres[0] arrApellidos[0]</p>
EOD;

$pdf->writeHTMLCell(0,0,'','',$tabla, 0, 0, false, 'L', true);

/*$tabla2 = <<<EOD
  <table border="1" style="font-size:12;font-weight:bold;">
    <tr>
      <td>$creador_nota</td>
    </tr>
  </table>
EOD;

$pdf->writeHTML($tabla2, true, false, true, false, "");*/

ob_end_clean();

$numero_rpt = rand(50,10000);
$pdf->Output("NotaDeEntrega.pdf", 'I');

