<?php {

    session_start();
    include_once("../../../".$_SESSION['FDSoil']."/packs/xtpl/xtemplate.class.php");
    $xtpl = new XTemplate('view.html');
    $xtpl->assign('FDSOIL', $_SESSION['FDSoil']); 
    $xtpl->assign('APPORG', $_SESSION['appOrg']);
    
    $aRif['rif'] = $_SESSION['rif'];
    include("../../../".$_SESSION['myApp']."/class/monitoreo.model.php");
    $obj = new monitoreo();

    $classTR = 'lospare';
    $resProductos = $obj->ListarProductos();
    while ($row = $obj->extraer_registro($resProductos)) {
            $xtpl->assign('CLASS_TR', $classTR);
            $xtpl->assign('ID', $row[0]);
	    $xtpl->assign('PRODUCTOS', $row[1]);
            $xtpl->parse('main.nivel_0');
        $classTR = ($classTR == 'lospare') ? 'losnone' : 'lospare';
    }
    
    $xtpl->parse('main');
    $xtpl->out('main');
}
