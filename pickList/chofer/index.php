<?php {

    session_start();
    include_once("../../../".$_SESSION['FDSoil']."/packs/xtpl/xtemplate.class.php");
    $xtpl = new XTemplate('view.html');
    $xtpl->assign('FDSOIL', $_SESSION['FDSoil']); 
    $xtpl->assign('APPORG', $_SESSION['appOrg']);
    include("../../../".$_SESSION['myApp']."/class/guia.model.php");
    $obj = new guia();    
    $classTR = 'lospare';
    $resChofer = $obj->listarChoferes($_GET);
    while ($row = $obj->extraer_arreglo($resChofer)) {
            $xtpl->assign('CLASS_TR', $classTR);
            $xtpl->assign('ID', $row[0]);
	    $xtpl->assign('CEDULA', $row[1]);
            $xtpl->assign('DESCRIPCION', $row[2]);
	    //$xtpl->assign('FLOTILLA', $row[3]); 
            $xtpl->parse('main.nivel_0');
        $classTR = ($classTR == 'lospare') ? 'losnone' : 'lospare';
    }
    
    $xtpl->parse('main');
    $xtpl->out('main');
}
