<?php {

    session_start();
    include_once("../../../".$_SESSION['FDSoil']."/packs/xtpl/xtemplate.class.php");
    $xtpl = new XTemplate('view.html');
    $xtpl->assign('FDSOIL', $_SESSION['FDSoil']); 
    $xtpl->assign('APPORG', $_SESSION['appOrg']);    
    include("../../../".$_SESSION['myApp']."/class/guia.model.php");
    $obj = new guia();    
    $classTR = 'lospare';
    $resVehiculo = $obj->listarPlataforma($_GET);
    while ($row = $obj->extraer_arreglo($resVehiculo)) {
            $xtpl->assign('CLASS_TR', $classTR);
            $xtpl->assign('ID', $row[0]);
	    $xtpl->assign('PLACA', $row[1]);
            $xtpl->assign('TIPO', $row[2]);
            $xtpl->assign('MARCA', $row[3]);
            $xtpl->assign('MODELO', $row[4]);
            $xtpl->parse('main.nivel_0');
        $classTR = ($classTR == 'lospare') ? 'losnone' : 'lospare';
    }
    
    $xtpl->parse('main');
    $xtpl->out('main');
}
