<?php
use \myApp\Minuta\MinutaAsistente as MinutaAsistente;

class SubIndex
{

    public function __construct($method)
    {
        $_POST = \FDSoil\Func::base64DecodeArrValKey($_POST);
        self::$method();
    }

    private function minutaAsistenteRegister() { echo base64_encode(MinutaAsistente::minutaAsistenteRegister()); }

    private function minutaAsistenteGet() { echo base64_encode(json_encode(MinutaAsistente::minutaAsistenteGet())); }

    private function minutaAsistenteDelete() { echo base64_encode(MinutaAsistente::minutaAsistenteDelete()); }

}

