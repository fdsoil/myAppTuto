<?php
use \myApp\Minuta\MinutaAcuerdo as MinutaAcuerdo;

class SubIndex
{

    public function __construct($method)
    {
        $_POST = \FDSoil\Func::base64DecodeArrValKey($_POST);
        self::$method();
    }

    private function minutaAcuerdoRegister() { echo base64_encode(MinutaAcuerdo::minutaAcuerdoRegister()); }

    private function minutaAcuerdoGet() { echo base64_encode(json_encode(MinutaAcuerdo::minutaAcuerdoGet())); }

    private function minutaAcuerdoDelete() { echo base64_encode(MinutaAcuerdo::minutaAcuerdoDelete()); }

}

