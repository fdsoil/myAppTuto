<?php
use \myApp\Minuta\MinutaPlanteamiento as MinutaPlanteamiento;

class SubIndex
{

    public function __construct($method)
    {
        $_POST = \FDSoil\Func::base64DecodeArrValKey($_POST);
        self::$method();
    }

    private function minutaPlanteamientoRegister() { echo base64_encode(MinutaPlanteamiento::minutaPlanteamientoRegister()); }

    private function minutaPlanteamientoGet() { echo base64_encode(json_encode(MinutaPlanteamiento::minutaPlanteamientoGet())); }

    private function minutaPlanteamientoDelete() { echo base64_encode(MinutaPlanteamiento::minutaPlanteamientoDelete()); }

}

