<?php
class Model
{

    public function dataGet()
    {
        $resp = '';
        if ($_POST['id']=='111111' && $_POST['act'])
            $resp='Fulano, De Tal.|fdtal@dominio.ext';
        else if ($_POST['id']=='222222' && !$_POST['act'])
            $resp='Fulgencio, Mengano.|fmengano@dominio.ext';
        else if ($_POST['id']=='333333' && $_POST['act'])
            $resp='Perenceo, Perez.|pperez@dominio.ext';
        else
            $resp='Disculpe: No existe información asociada.|';
        return $resp;
    }

    public function municipioGet()
    {
        switch ( $_POST['id'] ) {
            case '1':
                return [ [ 0=>1, 1=>'GIRARDOT'         ],
                         [ 0=>2, 1=>'JOSÉ FÉLIX RIBAS' ],
                         [ 0=>3, 1=>'SAN CASIMIRO'     ] ];
                break;
            case '2':
                return [ [  0=>4,   1=>'GUACARA'        ],
                         [  0=>5,   1=>'SAN JOAQUÍN'    ],
                         [  0=>6,   1=>'PUERTO CABELLO' ] ];
                break;
            case '3':
                return [ [ 0=>7,   1=>'BARUTA'     ],
                         [ 0=>8,   1=>'CHACAO'     ],
                         [ 0=>9,   1=>'EL HATILLO' ] ];
                break;
        }
    }

    public function informatGet()
    {        
        return [ [ 0=>'111111', 1=>'Fulano, De Tal.'    , 2=>'fdtal@dominio.ext'   , 3=>'Si' ],   
                 [ 0=>'222222', 1=>'Fulgencio, Mengano.', 2=>'fmengano@dominio.ext', 3=>'No' ],   
                 [ 0=>'333333', 1=>'Perenceo, Perez.'   , 2=>'pperez@dominio.ext'  , 3=>'Si' ] ];
    }

    public function facturaMasterGet()
    {        
        return [ 'num_factura'  =>  '0000001', 
                     'fec_factura'  =>  '06/09/2018', 
                     'cliente'      =>  'Fulano, De Tal.'];
    }
    
    public function facturaDetailGet()
    {
        return [ [ 'cod'=>'00123', 'producto'=>'ALICATE' , 'precio'=>234.3 , 'cant'=>2   ],   
                 [ 'cod'=>'00295', 'producto'=>'CABLE'   , 'precio'=>54    , 'cant'=>50  ],   
                 [ 'cod'=>'00381', 'producto'=>'TORNILLO', 'precio'=>9.5   , 'cant'=>200 ] ];
    }

}
 
