<?php

require_once __DIR__.'/Model.php';

class SubIndex
{

    public function __construct($method) { self::$method(); }

    private function ajaxSimple() { echo Model::dataGet(); }

    private function toFillCombo() { echo json_encode(Model::municipioGet()); }

    private function toFillTable() { echo \FDSoil\Func::matrixToString(Model::informatGet(),'|',"#"); }

    private function toFillForm()
    {
        $data['objeto'] = Model::facturaMasterGet();
        $data['objeto']['aux'] = Model::facturaDetailGet();
        echo json_encode($data);
    }

}

