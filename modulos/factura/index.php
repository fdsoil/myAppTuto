<?php
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        Func::btnRecordAdd( $xtpl ,["btnRecordName"=>"Facturas"]);
        $result = \myApp\Factura::facturaGet('LIST');
        while ($row = \FDSoil\DbFunc::fetchAssoc($result)) {
            $xtpl->assign('NUMERO', $row['numero']);
	    $xtpl->assign('FEC_FAC', $row['fec_fac']);
	    $xtpl->assign('CLIENTE', $row['cliente']);
            Func::btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            Func::btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);	
	    $xtpl->parse('main.rows');
        }
        Func::btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView; 
    }
}
