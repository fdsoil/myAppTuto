<?php
use \FDSoil\Func as Func;

class SubIndex
{

    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = json_encode([json_decode($_SESSION['menu']), self::arrayGet()]);
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        Func::btnsPutPanel( $xtpl, [["btnName"=>"Exit"]]);     
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
    /////////////////////////////////////////////////////////////////////////////
    private function arrayGet()                                                //
    {                                                                          //
        return array(                                                          //
                    array(  0=>'1',   1=>'Item_1', 2=>'1', 3=>'SubItem_1_1' ), //   
                    array(  0=>'1',   1=>'Item_1', 2=>'2', 3=>'SubItem_1_2' ), // 
                    array(  0=>'2',   1=>'Item_2', 2=>'3', 3=>'SubItem_2_1' ), //   
                    array(  0=>'2',   1=>'Item_2', 2=>'4', 3=>'SubItem_2_2' ), //   
                    array(  0=>'2',   1=>'Item_2', 2=>'5', 3=>'SubItem_2_3' ), //   
                    array(  0=>'2',   1=>'Item_2', 2=>'6', 3=>'SubItem_2_4' ), //
                    array(  0=>'3',   1=>'Item_3', 2=>'7', 3=>'SubItem_3_1' ), //   
                    array(  0=>'3',   1=>'Item_3', 2=>'8', 3=>'SubItem_3_2' ), //   
                    array(  0=>'3',   1=>'Item_3', 2=>'9', 3=>'SubItem_3_3' )  //
                );                                                             //
    }                                                                          //
    /////////////////////////////////////////////////////////////////////////////
}
