<?php
use \FDSoil\XTemplate as XTemplate;
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;
use \myApp\Persona as Persona;

class SubIndex
{
    public function execute($aReqs)
    {
        $obj = new Persona();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = [];
        $xtpl = new XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        $aRegist = array_key_exists('id', $_POST)
            ? DbFunc::fetchAssoc(Persona::personaGet('REGIST'))
                :\FDSoil\DbFunc::iniRegist('persona','persona');
        $xtpl->assign('ID', $aRegist['id']);
        $xtpl->assign('PASAPORTE', $aRegist['pasaporte']);
        $xtpl->assign('PASAPORTE_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('APELLIDOS', $aRegist['apellidos']);
        $xtpl->assign('NOMBRES', $aRegist['nombres']);
        $xtpl->assign('ID_PERSONA_TIPO', $aRegist['id_persona_tipo']);
        $xtpl->assign('FEC_NAC', Func::change_date_format($aRegist['fec_nac']));
        $xtpl->assign('SEXO', $aRegist['sexo']);
        $xtpl->assign('TRAYECTORIA_CHK', $aRegist['trayectoria'] === 't' ? 'checked' : '');
        $xtpl->assign('EXPLICACION', $aRegist['explicacion']);
        $result = Persona::personaTipoList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_PERSONA_TIPO', $row[0]);
            $xtpl->assign('DES_PERSONA_TIPO', $row[1]);
            $xtpl->assign('SELECTED_PERSONA_TIPO', ($aRegist['id_persona_tipo'] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.persona_tipo');
        }
        Func::btnsPutPanel( $xtpl, [["btnName" => "Return", "btnBack" => "persona"],
                                    ["btnName" => "Save"  , "btnClick"=> "valEnvio();"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}