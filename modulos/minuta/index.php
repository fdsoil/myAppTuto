<?php
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        Func::btnRecordAdd( $xtpl ,["btnRecordName"=>"Minuta"]);
        $result = \myApp\Minuta::minutaGet('LIST');
        while ($row = DbFunc::fetchAssoc($result)){
            $xtpl->assign('ID_CIUDAD', $row['id_ciudad']);
            $xtpl->assign('FECHA', Func::change_date_format($row['fecha']));
            $xtpl->assign('HORA', Func::timeMilitarToNormal($row['hora']));
            $xtpl->assign('LUGAR', $row['lugar']);
            $xtpl->assign('ID_ENTE', $row['id_ente']);
            $xtpl->assign('ID_DEPENDENCIA', $row['id_dependencia']);
            $xtpl->assign('ASUNTO', $row['asunto']);
            $xtpl->assign('MOTIVO', $row['motivo']);
            $xtpl->assign('OBSERVACION', $row['observacion']);
            $xtpl->assign('DES_CIUDAD', $row['des_ciudad']);
            $xtpl->assign('DES_DEPENDENCIA', $row['des_dependencia']);
            $xtpl->assign('DES_ENTE', $row['des_ente']);
            Func::btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            Func::btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);
            $xtpl->parse('main.rows');
        }
        Func::btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

