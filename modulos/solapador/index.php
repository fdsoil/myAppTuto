<?php
use \FDSoil\Func as Func;
use \FDSoil\XTemplate as XTemplate;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $aSolapa[0] = self::_solapa0();
        $aSolapa[1] = self::_solapa1();
        $aSolapa[2] = self::_solapa2();
        $xtpl = new XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        for ($i = 0; $i < count($aSolapa); $i++) 
            $xtpl->assign('SOLAPA_'.$i, $aSolapa[$i]);   
        Func::btnsPutPanel( $xtpl, [["btnName"=>"Exit"]]);     
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
    ///////////////////////////////////////////////////////
    private function _solapa0()                          //
    {                                                    //
        $xtpl = new XTemplate(__DIR__."/solapa_0.html"); //
        $xtpl->assign('POSICION', 'PRIMERA');            //
        $xtpl->parse('main');                            //
        return $xtpl->out_var('main');                   //
    }                                                    //
                                                         //
    private function _solapa1()                          //
    {                                                    //
        $xtpl = new XTemplate(__DIR__."/solapa_1.html"); //
        $xtpl->assign('POSICION', 'SEGUNDA');            //
        $xtpl->parse('main');                            //
        return $xtpl->out_var('main');                   //
    }                                                    //
                                                         //
    private function _solapa2()                          //
    {                                                    //
        $xtpl = new XTemplate(__DIR__."/solapa_2.html"); //
        $xtpl->assign('POSICION', 'TERCERA');            //
        $xtpl->parse('main');                            //
        return $xtpl->out_var('main');                   //
    }                                                    //
    ///////////////////////////////////////////////////////
}
