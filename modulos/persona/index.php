<?php
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        Func::btnRecordAdd( $xtpl ,["btnRecordName"=>"Persona"]);
        $result = \myApp\Persona::personaGet('LIST');
        while ($row = DbFunc::fetchAssoc($result)){
            $xtpl->assign('PASAPORTE', $row['pasaporte']);
            $xtpl->assign('APELLIDOS', $row['apellidos']);
            $xtpl->assign('NOMBRES', $row['nombres']);
            $xtpl->assign('ID_PERSONA_TIPO', $row['id_persona_tipo']);
            $xtpl->assign('FEC_NAC', Func::change_date_format($row['fec_nac']));
            $xtpl->assign('SEXO', $row['sexo']);
            $xtpl->assign('TRAYECTORIA', ($row['trayectoria']==='t')?'TRUE':'FALSE');
            $xtpl->assign('EXPLICACION', $row['explicacion']);
            $xtpl->assign('DES_PERSONA_TIPO', $row['des_persona_tipo']);
            Func::btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            Func::btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);
            $xtpl->parse('main.rows');
        }
        Func::btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

