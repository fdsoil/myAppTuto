function sendDataJson()
{
    let responseAjax = (response) =>
    {
        var obj=response.objeto;
        document.getElementById('id_cod_guia').innerHTML=obj.num_factura;
        document.getElementById('id_fecha_apertura').innerHTML=obj.fec_factura;
        document.getElementById('id_cliente').innerHTML=obj.cliente.toUpperCase();
        deleteAllRowsTable('id_tab',1);
        fillTableThatExistsJSON('id_tab',obj.aux);
        paintTRsClearDark('id_tab');
    }
    let ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + myApp + "/reqs/control/exAjax/index.php/toFillForm";
    ajax.funResponse = responseAjax;
    ajax.dataType = 'json';
    ajax.send();
}

