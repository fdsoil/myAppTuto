<?php
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute($aParams)
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        $matrix=self::fruitsGet();
        foreach ($matrix as $arr){
            $xtpl->assign('ID', $arr[0]);
            $xtpl->assign('DESCRIPCION',  $arr[1]);            
            $xtpl->parse('main.combo_multiple');
        }
        Func::btnsPutPanel( $xtpl, [["btnName"=>"Exit"],["btnName"=>"Save","btnClick"=>"save();"]]); 
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
    ///////////////////////////////////////////
    private function fruitsGet()             //
    {                                        //
        return  [ [ 0=>1,   1=>'Aguacate' ], //  
                  [ 0=>2,   1=>'Boniato'  ], //  
                  [ 0=>3,   1=>'Coco'     ], //
                  [ 0=>4,   1=>'Durazno'  ], //  
                  [ 0=>5,   1=>'Fresa'    ], //  
                  [ 0=>6,   1=>'Guanábana'], //
                  [ 0=>7,   1=>'Icaco'    ], //
                  [ 0=>8,   1=>'Kiwi'     ], //  
                  [ 0=>9,   1=>'Mango'    ], //
                  [ 0=>10,  1=>'Manzana'  ], //  
                  [ 0=>11,  1=>'Níspero'  ], //  
                  [ 0=>12,  1=>'Plátano'  ], //
                  [ 0=>13,  1=>'Pera'     ], //
                  [ 0=>14,  1=>'Uva'      ], //  
                  [ 0=>15,  1=>'Zapote'   ]  //
            ];                               //
    }                                        //
    ///////////////////////////////////////////
}

