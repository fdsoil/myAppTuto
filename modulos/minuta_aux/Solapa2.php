<?php
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;
use \myApp\Minuta\MinutaAsistente as MinutaAsistente;

trait Solapa2
{
    private function _solapa2()
    {
        $xtpl = new \FDSoil\XTemplate(__DIR__."/solapa2.html");
        Func::appShowId($xtpl);
        $result = MinutaAsistente::enteList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_ENTE', $row[0]);
            $xtpl->assign('DES_ENTE', $row[1]);
            $xtpl->parse('main.ente');
        }
        $result = MinutaAsistente::dependenciaList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_DEPENDENCIA', $row[0]);
            $xtpl->assign('DES_DEPENDENCIA', $row[1]);
            $xtpl->parse('main.dependencia');
        }
        $result = MinutaAsistente::cargoList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_CARGO', $row[0]);
            $xtpl->assign('DES_CARGO', $row[1]);
            $xtpl->parse('main.cargo');
        }
        if (array_key_exists('id', $_POST)) {
            $matrix = MinutaAsistente::minutaAsistenteGet();
            $classTR = 'lospare';
            foreach ($matrix as $arr) {
                $xtpl->assign('CLASS_TR', $classTR);
                $xtpl->assign('ID', $arr['id']);
                $xtpl->assign('CEDULA', $arr['cedula']);
                $xtpl->assign('NOMBRE', $arr['nombre']);
                $xtpl->assign('CORREO', $arr['correo']);
                $xtpl->assign('TELEFONO', $arr['telefono']);
                $xtpl->assign('OBSERVACION', $arr['observacion']);
                $xtpl->assign('ID_ENTE', $arr['id_ente']);
                $xtpl->assign('DES_ENTE', $arr['des_ente']);
                $xtpl->assign('ID_DEPENDENCIA', $arr['id_dependencia']);
                $xtpl->assign('DES_DEPENDENCIA', $arr['des_dependencia']);
                $xtpl->assign('ID_CARGO', $arr['id_cargo']);
                $xtpl->assign('DES_CARGO', $arr['des_cargo']);
                $xtpl->parse('main.tab_minuta_asistente');
                $classTR = ($classTR == 'losnone') ? 'lospare' : 'losnone';
            }
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

