function sendMinutaAcuerdoRegister() 
{
    var responseAjax = (response) =>
    {
        msjAdmin(response);
        if (inArray(response, ['C', 'A'])){
            sendMinutaAcuerdoGet();
            onOffPanelMinutaAcuerdo();
        }
    }
    var sIdVal = document.getElementById('id').value;
    var oPanel = document.getElementById('id_panel_minuta_acuerdo');
    var reqs = b64EncodeUnicode('id_minuta') + '=' + b64EncodeUnicode(sIdVal) + '&' + request(oPanel, true);
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + myApp + "/reqs/control/minuta/minuta_acuerdo.php/minutaAcuerdoRegister";
    ajax.data = reqs;
    ajax.funResponse = responseAjax;
    ajax.b64 = true;
    ajax.send();
}

function sendMinutaAcuerdoGet(bAsync)
{
    var responseAjax = (response) =>
    {
        fillTabMinutaAcuerdo(response);
        valBtnClose();
    }
    var reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(document.getElementById("id").value);
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + myApp + "/reqs/control/minuta/minuta_acuerdo.php/minutaAcuerdoGet";
    ajax.data = reqs;
    ajax.funResponse = responseAjax;
    ajax.dataType = 'json';
    ajax.async = bAsync;
    ajax.b64 = true;
    ajax.send();
}

function sendMinutaAcuerdoDelete(id)
{
    confirm('Desea Eliminar Este Registro?', 'Confirmar', function(ok) {
        if (ok) {
            var responseAjax = (response) =>
            {
                (response != 'B') ? msjAdmin(response) : sendMinutaAcuerdoGet();
            }
            var ajax = new sendAjax();
            ajax.method = 'POST';
            ajax.url = "../../../" + myApp + "/reqs/control/minuta/minuta_acuerdo.php/minutaAcuerdoDelete";
            ajax.data = b64EncodeUnicode("id")+"="+b64EncodeUnicode(id);
            ajax.funResponse = responseAjax;
            ajax.b64 = true;
            ajax.send();
        }
    });
}

