function sendMinutaPlanteamientoRegister() 
{
    var responseAjax = (response) =>
    {
        msjAdmin(response);
        if (inArray(response, ['C', 'A'])){
            sendMinutaPlanteamientoGet();
            onOffPanelMinutaPlanteamiento();
        }
    }
    var sIdVal = document.getElementById('id').value;
    var oPanel = document.getElementById('id_panel_minuta_planteamiento');
    var reqs = b64EncodeUnicode('id_minuta') + '=' + b64EncodeUnicode(sIdVal) + '&' + request(oPanel, true);
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + myApp + "/reqs/control/minuta/minuta_planteamiento.php/minutaPlanteamientoRegister";
    ajax.data = reqs;
    ajax.funResponse = responseAjax;
    ajax.b64 = true;
    ajax.send();
}

function sendMinutaPlanteamientoGet(bAsync)
{
    var responseAjax = (response) =>
    {
        fillTabMinutaPlanteamiento(response);
        valBtnClose();
    }
    var reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(document.getElementById("id").value);
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + myApp + "/reqs/control/minuta/minuta_planteamiento.php/minutaPlanteamientoGet";
    ajax.data = reqs;
    ajax.funResponse = responseAjax;
    ajax.dataType = 'json';
    ajax.async = bAsync;
    ajax.b64 = true;
    ajax.send();
}

function sendMinutaPlanteamientoDelete(id)
{
    confirm('Desea Eliminar Este Registro?', 'Confirmar', function(ok) {
        if (ok) {
            var responseAjax = (response) =>
            {
                (response != 'B') ? msjAdmin(response) : sendMinutaPlanteamientoGet();
            }
            var ajax = new sendAjax();
            ajax.method = 'POST';
            ajax.url = "../../../" + myApp + "/reqs/control/minuta/minuta_planteamiento.php/minutaPlanteamientoDelete";
            ajax.data = b64EncodeUnicode("id")+"="+b64EncodeUnicode(id);
            ajax.funResponse = responseAjax;
            ajax.b64 = true;
            ajax.send();
        }
    });
}

