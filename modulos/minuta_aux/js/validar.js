function valEnvio(sStatus)
{
    if (validateObjs(document.getElementById('div0')))
        sendMinutaRegister(sStatus);

    /*if(validateObjs(document.getElementById('div0'))) {
        * var dataDorm = request(document.getElementById('div_input_hidden'));
        * dataForm += '&'+request(document.getElementById('div0'));
        * dataForm += '&'+request(document.getElementById('div1'));
        * dataForm += '&'+request(document.getElementById('div2'));
        * dataForm += '&'+request(document.getElementById('div3'));
        * sendDataForm(dataForm);
    }*/
}

function botonGuardarOnOff(nTab)
{
    if (nTab==0)
        document.getElementById("id_guardar").style.display='';
    else
        document.getElementById("id_guardar").style.display='none';
}

function regresarTabBotonGuardarOnOff(tTab)
{
    for (var i=tTab-1; i >= 0; i--) {
        if (document.all('div'+i).style.display!="none") {
            if (i==0)
                document.getElementById("id_guardar").style.display='';
            else
                document.getElementById("id_guardar").style.display='none';
        }
    }
}

function segirBotonGuardarOnOff(tTab)
{
    for (var i=1;i<=tTab-1;i++) {
        if (document.all('div'+i).style.display!="none") {
            if (i==0)
                document.getElementById("id_guardar").style.display='';
            else
                document.getElementById("id_guardar").style.display='none';
        }
    }
}

function valBtnClose()
{
    document.getElementById('id_cerrar').style.display=(
        validateObjs(document.getElementById('div0'))
            && valTable(document.getElementById('id_tab_minuta_acuerdo'),1)
            && valTable(document.getElementById('id_tab_minuta_asistente'),1)
            && valTable(document.getElementById('id_tab_minuta_planteamiento'),1)
        )?'':'none';
}

function eventListenerActivar()
{
    document.getElementById("div0").addEventListener("change", valBtnClose);
    document.getElementById("div0").addEventListener("keyup", valBtnClose);
}

function valPrint()
{
    var valor = document.getElementById('id').value;
    show('id_cerrar',500);
    if ( valor !== "0" )
        relocateBlank('../../../'+myApp+'/reportes/prueba/prueba3/', {'id':valor}); 
}
