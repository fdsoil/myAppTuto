function onOffPanelMinutaAcuerdo() 
{
    toggle('id_panel_minuta_acuerdo', 'blind',500);
    changeTheObDeniedWritingImg('id_img_minuta_acuerdo');
    initObjs(document.getElementById('id_panel_minuta_acuerdo'));
    document.getElementById('id_minuta_acuerdo').value=0;
}

function valEnvioMinutaAcuerdo()
{
    var oDiv=document.getElementById('id_panel_minuta_acuerdo');
    if (validateObjs(document.getElementById("id_panel_minuta_acuerdo"))) 
        sendMinutaAcuerdoRegister();
}

function editMinutaAcuerdo(obj)
{
    if(document.getElementById('id_panel_minuta_acuerdo').style.display=='none'){
        onOffPanelMinutaAcuerdo();
    }
    document.getElementById('id_minuta_acuerdo').value=obj.parentNode.parentNode.id;
    var oPanel=document.getElementById("id_panel_minuta_acuerdo");
    var oInputs=oPanel.getElementsByTagName("input");
    oInputs['responsable'].value = obj.parentNode.parentNode.cells[0].innerHTML;
    var oSelects=oPanel.getElementsByTagName("select");
    var oTextAreas=oPanel.getElementsByTagName("textarea");
    oTextAreas['acuerdo'].value = obj.parentNode.parentNode.cells[1].innerHTML;
    oTextAreas['observacion'].value = obj.parentNode.parentNode.cells[2].innerHTML;
}

