function onOffPanelMinutaAsistente() 
{
    toggle('id_panel_minuta_asistente', 'blind',500);
    changeTheObDeniedWritingImg('id_img_minuta_asistente');
    initObjs(document.getElementById('id_panel_minuta_asistente'));
    document.getElementById('id_minuta_asistente').value=0;
}

function valEnvioMinutaAsistente()
{
    var oDiv=document.getElementById('id_panel_minuta_asistente');
    if (validateObjs(document.getElementById("id_panel_minuta_asistente"))) 
        sendMinutaAsistenteRegister();
}

function editMinutaAsistente(obj)
{
    if(document.getElementById('id_panel_minuta_asistente').style.display=='none'){
        onOffPanelMinutaAsistente();
    }
    document.getElementById('id_minuta_asistente').value=obj.parentNode.parentNode.id;
    var oPanel=document.getElementById("id_panel_minuta_asistente");
    var oInputs=oPanel.getElementsByTagName("input");
    oInputs['cedula'].value = obj.parentNode.parentNode.cells[0].innerHTML;
    oInputs['nombre'].value = obj.parentNode.parentNode.cells[1].innerHTML;
    oInputs['correo'].value = obj.parentNode.parentNode.cells[2].innerHTML;
    oInputs['telefono'].value = obj.parentNode.parentNode.cells[3].innerHTML;
    var oSelects=oPanel.getElementsByTagName("select");
    oSelects['id_ente'].value = obj.parentNode.parentNode.cells[4].id;
    oSelects['id_dependencia'].value = obj.parentNode.parentNode.cells[5].id;
    oSelects['id_cargo'].value = obj.parentNode.parentNode.cells[6].id;
    var oTextAreas=oPanel.getElementsByTagName("textarea");
    oTextAreas['observacion'].value = obj.parentNode.parentNode.cells[7].innerHTML;
}

