function fillTabMinutaAsistente(aObjs)
{

    var tbl = document.getElementById('id_tab_minuta_asistente');
    deleteAllRowsTable('id_tab_minuta_asistente',1);

    if (!tbl.tBodies[0]) {
        var tbody = document.createElement('tbody');
        tbody.setAttribute('style', 'display: block');
        tbody.setAttribute('style', 'width: 100%');
        tbl.appendChild(tbody);
    }

    if (aObjs.length !== 0) {
        for (var i = 0; i < aObjs.length; i++)
            tbl.tBodies[0].appendChild(fillTabAux(aObjs[i]));
        //setAttributeTD('id_tab_minuta_asistente','align','left', 1, 3, 1, aObjs.length+1);
        paintTRsClearDark('id_tab_minuta_asistente');
    }

    function fillTabAux(row)
    {

        var oTr = document.createElement('tr');
        oTr.id = row.id;
        var aTd = [];

        aTd[0] = document.createElement('td');
        aTd[0].appendChild(document.createTextNode(row.cedula));
        oTr.appendChild(aTd[0]);

        aTd[1] = document.createElement('td');
        aTd[1].appendChild(document.createTextNode(row.nombre));
        oTr.appendChild(aTd[1]);

        aTd[2] = document.createElement('td');
        aTd[2].appendChild(document.createTextNode(row.correo));
        oTr.appendChild(aTd[2]);

        aTd[3] = document.createElement('td');
        aTd[3].appendChild(document.createTextNode(row.telefono));
        oTr.appendChild(aTd[3]);

        aTd[4] = document.createElement('td');
        aTd[4].id = row.id_ente
        aTd[4].appendChild(document.createTextNode(row.des_ente));
        oTr.appendChild(aTd[4]);

        aTd[5] = document.createElement('td');
        aTd[5].id = row.id_dependencia
        aTd[5].appendChild(document.createTextNode(row.des_dependencia));
        oTr.appendChild(aTd[5]);

        aTd[6] = document.createElement('td');
        aTd[6].id = row.id_cargo
        aTd[6].appendChild(document.createTextNode(row.des_cargo));
        oTr.appendChild(aTd[6]);

        aTd[7] = document.createElement('td');
        aTd[7].appendChild(document.createTextNode(row.observacion));
        oTr.appendChild(aTd[7]);

        var oImgEdit = document.createElement('img');
        oImgEdit.setAttribute('class', 'accion');
        oImgEdit.setAttribute('src', '../../../../../'+appOrg+'/img/edit.png');
        oImgEdit.setAttribute('title', 'Editar datos...');
        oImgEdit.setAttribute('onclick',"editMinutaAsistente(this);valBtnClose();");
        aTd[8] = document.createElement('td');
        aTd[8].align = 'center';
        aTd[8].appendChild(oImgEdit);

        var oImgDelete = document.createElement('img');
        oImgDelete.setAttribute('class', 'accion');
        oImgDelete.setAttribute('src', '../../../../../'+appOrg+'/img/cross.png');
        oImgDelete.setAttribute('title', 'Eliminar/Borrar datos...');
        oImgDelete.setAttribute('onclick',"sendMinutaAsistenteDelete("+row.id+");valBtnClose();");
        aTd[8].appendChild(oImgDelete);
        oTr.appendChild(aTd[8]);

        return oTr;

    }

}