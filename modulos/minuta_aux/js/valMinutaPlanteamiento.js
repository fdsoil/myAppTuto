function onOffPanelMinutaPlanteamiento() 
{
    toggle('id_panel_minuta_planteamiento', 'blind',500);
    changeTheObDeniedWritingImg('id_img_minuta_planteamiento');
    initObjs(document.getElementById('id_panel_minuta_planteamiento'));
    document.getElementById('id_minuta_planteamiento').value=0;
}

function valEnvioMinutaPlanteamiento()
{
    var oDiv=document.getElementById('id_panel_minuta_planteamiento');
    if (validateObjs(document.getElementById("id_panel_minuta_planteamiento"))) 
        sendMinutaPlanteamientoRegister();
}

function editMinutaPlanteamiento(obj)
{
    if(document.getElementById('id_panel_minuta_planteamiento').style.display=='none'){
        onOffPanelMinutaPlanteamiento();
    }
    document.getElementById('id_minuta_planteamiento').value=obj.parentNode.parentNode.id;
    var oPanel=document.getElementById("id_panel_minuta_planteamiento");
    var oInputs=oPanel.getElementsByTagName("input");
    oInputs['ponente'].value = obj.parentNode.parentNode.cells[0].innerHTML;
    var oSelects=oPanel.getElementsByTagName("select");
    var oTextAreas=oPanel.getElementsByTagName("textarea");
    oTextAreas['planteamiento'].value = obj.parentNode.parentNode.cells[1].innerHTML;
    oTextAreas['observacion'].value = obj.parentNode.parentNode.cells[2].innerHTML;
}

