function sendMinutaAsistenteRegister() 
{
    var responseAjax = (response) =>
    {
        msjAdmin(response);
        if (inArray(response, ['C', 'A'])){
            sendMinutaAsistenteGet();
            onOffPanelMinutaAsistente();
        }
    }
    var sIdVal = document.getElementById('id').value;
    var oPanel = document.getElementById('id_panel_minuta_asistente');
    var reqs = b64EncodeUnicode('id_minuta') + '=' + b64EncodeUnicode(sIdVal) + '&' + request(oPanel, true);
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + myApp + "/reqs/control/minuta/minuta_asistente.php/minutaAsistenteRegister";
    ajax.data = reqs;
    ajax.funResponse = responseAjax;
    ajax.b64 = true;
    ajax.send();
}

function sendMinutaAsistenteGet(bAsync)
{
    var responseAjax = (response) =>
    {
        fillTabMinutaAsistente(response);
        valBtnClose();
    }
    var reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(document.getElementById("id").value);
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + myApp + "/reqs/control/minuta/minuta_asistente.php/minutaAsistenteGet";
    ajax.data = reqs;
    ajax.funResponse = responseAjax;
    ajax.dataType = 'json';
    ajax.async = bAsync;
    ajax.b64 = true;
    ajax.send();
}

function sendMinutaAsistenteDelete(id)
{
    confirm('Desea Eliminar Este Registro?', 'Confirmar', function(ok) {
        if (ok) {
            var responseAjax = (response) =>
            {
                (response != 'B') ? msjAdmin(response) : sendMinutaAsistenteGet();
            }
            var ajax = new sendAjax();
            ajax.method = 'POST';
            ajax.url = "../../../" + myApp + "/reqs/control/minuta/minuta_asistente.php/minutaAsistenteDelete";
            ajax.data = b64EncodeUnicode("id")+"="+b64EncodeUnicode(id);
            ajax.funResponse = responseAjax;
            ajax.b64 = true;
            ajax.send();
        }
    });
}

