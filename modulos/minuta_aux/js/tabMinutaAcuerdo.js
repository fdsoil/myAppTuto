function fillTabMinutaAcuerdo(aObjs)
{

    var tbl = document.getElementById('id_tab_minuta_acuerdo');
    deleteAllRowsTable('id_tab_minuta_acuerdo',1);

    if (!tbl.tBodies[0]) {
        var tbody = document.createElement('tbody');
        tbody.setAttribute('style', 'display: block');
        tbody.setAttribute('style', 'width: 100%');
        tbl.appendChild(tbody);
    }

    if (aObjs.length !== 0) {
        for (var i = 0; i < aObjs.length; i++)
            tbl.tBodies[0].appendChild(fillTabAux(aObjs[i]));
        //setAttributeTD('id_tab_minuta_acuerdo','align','left', 1, 3, 1, aObjs.length+1);
        paintTRsClearDark('id_tab_minuta_acuerdo');
    }

    function fillTabAux(row)
    {

        var oTr = document.createElement('tr');
        oTr.id = row.id;
        var aTd = [];

        aTd[0] = document.createElement('td');
        aTd[0].appendChild(document.createTextNode(row.responsable));
        oTr.appendChild(aTd[0]);

        aTd[1] = document.createElement('td');
        aTd[1].appendChild(document.createTextNode(row.acuerdo));
        oTr.appendChild(aTd[1]);

        aTd[2] = document.createElement('td');
        aTd[2].appendChild(document.createTextNode(row.observacion));
        oTr.appendChild(aTd[2]);

        var oImgEdit = document.createElement('img');
        oImgEdit.setAttribute('class', 'accion');
        oImgEdit.setAttribute('src', '../../../../../'+appOrg+'/img/edit.png');
        oImgEdit.setAttribute('title', 'Editar datos...');
        oImgEdit.setAttribute('onclick',"editMinutaAcuerdo(this);valBtnClose();");
        aTd[3] = document.createElement('td');
        aTd[3].align = 'center';
        aTd[3].appendChild(oImgEdit);

        var oImgDelete = document.createElement('img');
        oImgDelete.setAttribute('class', 'accion');
        oImgDelete.setAttribute('src', '../../../../../'+appOrg+'/img/cross.png');
        oImgDelete.setAttribute('title', 'Eliminar/Borrar datos...');
        oImgDelete.setAttribute('onclick',"sendMinutaAcuerdoDelete("+row.id+");valBtnClose();");
        aTd[3].appendChild(oImgDelete);
        oTr.appendChild(aTd[3]);

        return oTr;

    }

}