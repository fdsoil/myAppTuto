<?php
use \FDSoil\XTemplate as XTemplate;
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;
use \myApp\Minuta as Minuta;

trait Solapa0
{
    private function _solapa0()
    {
        $aRegist = array_key_exists('id', $_POST) ?
            DbFunc::fetchAssoc(Minuta::minutaGet('REGIST')) :
                \FDSoil\DbFunc::iniRegist('minuta','minuta');
        $xtpl = new \FDSoil\XTemplate(__DIR__."/solapa0.html");
        Func::appShowId($xtpl);
        $xtpl->assign('ID', $aRegist['id']);
        $xtpl->assign('FECHA', Func::change_date_format($aRegist['fecha']));
        $xtpl->assign('HORA', Func::timeMilitarToNormal($aRegist['hora']));
        $xtpl->assign('LUGAR', $aRegist['lugar']);
        $xtpl->assign('ASUNTO', $aRegist['asunto']);
        $xtpl->assign('ASUNTO_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('MOTIVO', $aRegist['motivo']);
        $xtpl->assign('OBSERVACION', $aRegist['observacion']);
        $result = Minuta::ciudadList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_CIUDAD', $row[0]);
            $xtpl->assign('DES_CIUDAD', $row[1]);
            $xtpl->assign('SELECTED_CIUDAD', ($aRegist['id_ciudad'] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.ciudad');
        }
        $result = Minuta::dependenciaList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_DEPENDENCIA', $row[0]);
            $xtpl->assign('DES_DEPENDENCIA', $row[1]);
            $xtpl->assign('SELECTED_DEPENDENCIA', ($aRegist['id_dependencia'] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.dependencia');
        }
        $result = Minuta::enteList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_ENTE', $row[0]);
            $xtpl->assign('DES_ENTE', $row[1]);
            $xtpl->assign('SELECTED_ENTE', ($aRegist['id_ente'] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.ente');
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

