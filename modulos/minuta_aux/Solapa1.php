<?php
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;
use \myApp\Minuta\MinutaAcuerdo as MinutaAcuerdo;

trait Solapa1
{
    private function _solapa1()
    {
        $xtpl = new \FDSoil\XTemplate(__DIR__."/solapa1.html");
        Func::appShowId($xtpl);
        if (array_key_exists('id', $_POST)) {
            $matrix = MinutaAcuerdo::minutaAcuerdoGet();
            $classTR = 'lospare';
            foreach ($matrix as $arr) {
                $xtpl->assign('CLASS_TR', $classTR);
                $xtpl->assign('ID', $arr['id']);
                $xtpl->assign('ACUERDO', $arr['acuerdo']);
                $xtpl->assign('RESPONSABLE', $arr['responsable']);
                $xtpl->assign('OBSERVACION', $arr['observacion']);
                $xtpl->parse('main.tab_minuta_acuerdo');
                $classTR = ($classTR == 'losnone') ? 'lospare' : 'losnone';
            }
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

