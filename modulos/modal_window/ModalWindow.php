<?php

trait ModalWindow
{
    private function _modalWindow()
    {
        $xtpl = new XTemplate(__DIR__."/modalWindow.html");
        \FDSoil\Func::appShowId($xtpl);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

