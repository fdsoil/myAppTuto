<?php

include_once(__DIR__."/Solapa0.php");
include_once(__DIR__."/Solapa1.php");
include_once(__DIR__."/ModalWindow.php");

class SubIndex
{
    use Solapa0, Solapa1, ModalWindow;

    private $_obj;
    private $_aRegist;
    private $_aView;
    private $_true;

    public function __construct()
    {
        require_once("../../../".$_SESSION['myApp']."/class/NotaEntrega.model.php");
        $this->_obj = new NotaEntrega();
        $this->_aView['include'] = $this->_obj->getFileJSON(__DIR__."/js/include.json");
        $this->_aView['userData'] = $this->_obj->usuarioData();
        $this->_aView['load'] = array_key_exists('id', $_POST)?$_POST['id']:0;
        if ( array_key_exists('id', $_POST) ) {
            $this->_aRegist=$this->_obj->extraer_asociativo($this->_obj->notaEntregaGet('REGIST'));
            $this->_true = ($this->_obj->notaEntregaBlockRecord() != 'false') ? true: false;
        } else
            $this->_true = false;

    }

    public function execute()
    {
        if ($this->_true) {
            $xtpl = new XTemplate(__DIR__."/view.html");
            $this->_obj->appShowId($xtpl);
            $this->_obj->CodFec(
                $xtpl,
                [
                    $this->_aRegist['numero'],
                    $this->_obj->change_date_format($this->_aRegist['fecha_nota_entrega'])
                ]
            );
            if ($this->_aRegist['id'] == 0) {
                $xtpl->assign('TAB_NONE_BLOCK1', 'none');
                $xtpl->assign('BOTONES_NONE_BLOCK', 'none');
            }
            $xtpl->assign('ID', $this->_aRegist['id']);
            $aSolapa[0] = self::_solapa0();
            $aSolapa[1] = self::_solapa1();
            $this->_obj->bldSolapas($xtpl, $aSolapa);
            $this->_obj->btnsPutPanel( $xtpl, [["btnName" => "Return", "btnUnBlock" => "unlLockAndExit();"],
                                                //["btnName" => "Save", "btnClick"=> "valEnvio();"],
                                                ["btnName" => "Close", "btnClick"=> "valClose();", "btnDisplay" => "none"]]);
            $xtpl->assign('MODAL_WINDOW', self::_modalWindow());
            $xtpl->parse('main');
            $this->_aView['content'] = $xtpl->out_var('main');
            return $this->_aView;
        } else
            header("Location: ../nota_entrega_enviar/");
    }
}
