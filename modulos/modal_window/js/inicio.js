function inicio(oJSON)
{
    initView(); 
    menu(oJSON);
}

function showModalWindow()
{
    show("div_deshacer_fondo_opaco",'clip',250);
    show("div_deshacer_modal",'clip',500);
}

function hideModalWindow()
{
    hide("div_deshacer_fondo_opaco",'clip',250);
    hide("div_deshacer_modal",'clip',500);
}
