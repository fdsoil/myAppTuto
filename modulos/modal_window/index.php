<?php
use \FDSoil\Func as Func;
include_once(__DIR__."/ModalWindow.php");

class SubIndex
{
    use ModalWindow;

    public function execute($aParams)
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        Func::btnExit($xtpl);
        $xtpl->assign('MODAL_WINDOW', self::_modalWindow());
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

