<?php
use \FDSoil\Func as Func;
use \FDSoil\DbFunc as DbFunc;
use \myApp\Factura as Factura;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = [];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        $aRegist = array_key_exists('id', $_POST)
            ? DbFunc::fetchAssoc(Factura::facturaGet('REGIST'))
                :DbFunc::iniRegist('factura','factura');
        $xtpl->assign('ID_FACTURA', $aRegist['id']);
        $xtpl->assign('NUMERO', $aRegist['numero']);
        $xtpl->assign('FEC_FAC', $aRegist['fec_fac']);
        $xtpl->assign('CLIENTE', $aRegist['cliente']);
        $result = Factura::listProducto();
        while ($row = DbFunc::fetchRow($result)) { 
            $xtpl->assign('ID', $row[0]);
            $xtpl->assign('DESCRIPCION', $row[2].'-'.$row[1].'-'.$row[3]);
            $xtpl->assign('CODIGO', $row[2]);
            $xtpl->assign('PRECIO', $row[3]);
            $xtpl->parse('main.producto');
        }    
        $result = Factura::facturaAuxGet();
        $classTR = 'lospare';
        while ($row = DbFunc::fetchAssoc($result)){
            $xtpl->assign('CLASS_TR', $classTR);
            $xtpl->assign('ID', $row['id']);
            $xtpl->assign('CODIGO_AUX', $row['codigo']);
            $xtpl->assign('DESCRIPCION_AUX', $row['descripcion']);
	    $xtpl->assign('PRECIO_AUX', number_format($row['precio'], 2, ',', '.'));
	    $xtpl->assign('CANTIDAD_AUX', number_format($row['cantidad'], 0, ',', '.'));
            $xtpl->assign('SUBTOTAL', number_format($row['precio']*$row['cantidad'], 2, ',', '.'));
            $classTR = ($classTR == 'losnone') ? 'lospare' : 'losnone';
	    $xtpl->parse('main.rows');
        }
        Func::btnsPutPanel( $xtpl, [["btnName"=>"Return", "btnBack" =>"factura"], 
                                    ["btnName"=>"Save"  , "btnClick"=>"valEnvio();"]]); 
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

