function addRegTab(){
    
    var objTab=document.getElementById('id_tab');
    var objProd=document.getElementById('id_producto');
    var objCant=document.getElementById('id_cantidad');
    var prodCod=objProd.options[objProd.selectedIndex].getAttribute('codigo');
    var prodText=objProd.options[objProd.selectedIndex].text;
    var prodPrec=objProd.options[objProd.selectedIndex].getAttribute('precio');
    
    var tr=document.createElement('tr');
    tr.id=objProd.value;

    var td0=document.createElement('td');
    var node0=document.createTextNode(prodCod);
    td0.appendChild(node0);
    td0.setAttribute('align','center');
    tr.appendChild(td0); 
    
    var td1=document.createElement('td');
    var node1=document.createTextNode(prodText);
    td1.appendChild(node1);
    tr.appendChild(td1); 
    
    var td2=document.createElement('td');
    var node2=document.createTextNode(putFloatFormat((prodPrec*1), '.',',',2));
    td2.setAttribute('align','right');
    td2.appendChild(node2);
    tr.appendChild(td2); 
    
    var td3=document.createElement('td');
    var node3=document.createTextNode(objCant.value);
    td3.setAttribute('align','right');
    td3.appendChild(node3);
    tr.appendChild(td3); 
    
    var td4=document.createElement('td');
    var node4=document.createTextNode(putFloatFormat((prodPrec*1)*(delFormat(objCant.value)*1),'.',',',2));
    td4.appendChild(node4);
    td4.setAttribute('align','right');
    tr.appendChild(td4); 
   
    var inputDelete = document.createElement('input');        
    inputDelete.setAttribute('type','image');
    inputDelete.setAttribute('onClick',"delRow('id_tab',this);paintTRsClearDark('id_tab');");//sweepThisTable('id_tab' , 1);
    inputDelete.setAttribute('src','../../../../../'+appOrg+'/img/cross.png');
    inputDelete.setAttribute('class','accion');
    inputDelete.setAttribute('title','Eliminar/Borrar datos...');    
    var td5 = document.createElement('td');
    td5.setAttribute('align','center');
    td5.appendChild(inputDelete);        
    tr.appendChild(td5);
    objTab.appendChild(tr); 
    paintTRsClearDark('id_tab');
}

function sweepThisTable() {
    var objTab = document.getElementById('id_tab');
    var strArray='';
    for (var i=1; i<objTab.rows.length; i++) {
        strArray+=objTab.rows[i].id+'|';
        strArray+=delFormat(objTab.rows[i].cells[2].innerHTML)+'|';
        strArray+=delFormat(objTab.rows[i].cells[3].innerHTML)+'#';
    }
    document.getElementById('id_str_array_hidden').value = strArray.substring(0,(strArray.length-1));
}

