<?php
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        $matrix=self::estadosGet();
        foreach ($matrix as $arr){
            $xtpl->assign('ID', $arr[0]);
            $xtpl->assign('DESCRIPCION',  $arr[1]);            
            $xtpl->parse('main.combo');
        }
        Func::btnExit($xtpl);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
    /////////////////////////////////////////
    private function estadosGet()          //
    {                                      //
        return [ [ 0=>1, 1=>'ARAGUA'  ],   // 
                 [ 0=>2, 1=>'CARABOBO'],   //
                 [ 0=>3, 1=>'MIRANDA' ] ]; //
    }                                      //
    /////////////////////////////////////////
}
