function inicio(oJSON){
    initView(); 
    menu(oJSON);
}

function mostrar(){
    var efecto= radioGetCheckedValue('efectos');
    (efecto=='default')?show('id_div',1000):show('id_div', efecto, 1000);
}

function ocultar(){
    var efecto= radioGetCheckedValue('efectos');
    (efecto=='default')?hide('id_div',1000):hide('id_div', efecto, 1000);
}

function cambiar(){
    var efecto= radioGetCheckedValue('efectos');
    (efecto=='default')?toggle('id_div',1000):toggle('id_div', efecto, 1000);
}

/*function radioGetCheckedValue(nameElements){
    var objRadio=document.getElementsByName(nameElements);
    var resp='';
        for(var i=0;i<objRadio.length;i++){
            if(objRadio[i].checked){
                 resp=objRadio[i].value;
                 break;
            }
        }
    return resp;
}*/

