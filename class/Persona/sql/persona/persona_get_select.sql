SELECT 
    A.id,
    A.pasaporte,
    A.apellidos,
    A.nombres,
    A.id_persona_tipo,
    A.fec_nac,
    A.sexo,
    A.trayectoria,
    A.explicacion
    ,B.descripcion AS des_persona_tipo
FROM persona.persona A
INNER JOIN persona.persona_tipo B ON A.id_persona_tipo=B.id
{fld:where}
ORDER BY 2;
