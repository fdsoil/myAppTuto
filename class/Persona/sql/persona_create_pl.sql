-- Function: persona.persona_register(integer, character varying, character varying, character varying, integer, date, character varying, boolean, text)

-- DROP FUNCTION persona.persona_register(integer, character varying, character varying, character varying, integer, date, character varying, boolean, text);

CREATE OR REPLACE FUNCTION persona.persona_register(i_id integer, i_pasaporte character varying, i_apellidos character varying, i_nombres character varying, i_id_persona_tipo integer, i_fec_nac date, i_sexo character varying, i_trayectoria boolean, i_explicacion text)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM persona.persona
                        WHERE pasaporte=i_pasaporte;
                IF  v_existe='f' THEN
                        INSERT INTO persona.persona(
                                pasaporte,
                                apellidos,
                                nombres,
                                id_persona_tipo,
                                fec_nac,
                                sexo,
                                trayectoria,
                                explicacion)
                        VALUES (
                                i_pasaporte,
                                i_apellidos,
                                i_nombres,
                                i_id_persona_tipo,
                                i_fec_nac,
                                i_sexo,
                                i_trayectoria,
                                i_explicacion);
                        o_return:= 'C';
                ELSE
                        o_return:= 'T';
                END IF;
        ELSE
                UPDATE persona.persona SET
                        apellidos=i_apellidos,
                        nombres=i_nombres,
                        id_persona_tipo=i_id_persona_tipo,
                        fec_nac=i_fec_nac,
                        sexo=i_sexo,
                        trayectoria=i_trayectoria,
                        explicacion=i_explicacion
                WHERE id=i_id;
                        o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION persona.persona_register(integer, character varying, character varying, character varying, integer, date, character varying, boolean, text)
  OWNER TO postgres;

-- Function: persona.persona_delete(integer)

-- DROP FUNCTION persona.persona_delete(integer);

CREATE OR REPLACE FUNCTION persona.persona_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM persona.persona WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION persona.persona_delete(integer)
  OWNER TO postgres;

