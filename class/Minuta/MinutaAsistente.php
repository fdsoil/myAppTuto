<?php
namespace myApp\Minuta;

use \FDSoil\Func as Func;
use \FDSoil\DbFunc as DbFunc;

/** MinutaAsistente: Clase para actualizar y consultar la tabla 'minuta_asistente'.
* Utiliza '\FDSoil\Func' y '\FDSoil\DbFunc' respectivamente.*/
class MinutaAsistente
{

    /** Devuelve la ruta en que están ubicados los archivos .sql de la clase MinutaAsistente.
    * Descripción: Devuelve la ruta en que están ubicados los .sql (querys) de la clase MinutaAsistente.
    * @return string La ruta en que están ubicados los archivos .sql (querys) de la clase MinutaAsistente.*/
    private function _path() { return '../../../'.$_SESSION['myApp'].'/class/Minuta/sql/minuta_asistente/'; }

    /** Actualiza registro de la tabla 'minuta_asistente'.
    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla 'minuta_asistente'.
    * Nota: Requiere de los respectivos valores procedentes del $_POST para actualizar el registro.*/
    public function minutaAsistenteRegister()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'minuta_asistente_register_pl.sql',$_POST, true, 'ACTUALIZANDO REGISTRO'));
        return $row[0];
    }

    /** Obtener registro(s) de la tabla 'minuta_asistente'.
    * Descripción: Obtener registro(s) de la tabla 'minuta_asistente'.
    * Nota: Requiere el correspondiente valor $_POST identificativo del registro específico a consultar.
    * @return result Resultado con registro(s) de la tabla 'minuta_asistente'.*/
    public function minutaAsistenteGet()
    {
        return DbFunc::fetchAllAssoc(DbFunc::exeQryFile(self::_path().'minuta_asistente_get_select.sql', $_POST));
    }

    /** Elimina registro de la tabla 'minuta_asistente'.
    * Descripción: Realiza eliminación del registro ( delete ) de la tabla 'minuta_asistente'.
    * Nota: Requiere el valor $_POST identificativo para buscar y eliminar registro en base de datos.*/ 
    public function minutaAsistenteDelete()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'minuta_asistente_delete_pl.sql', $_POST, true, 'ELIMINANDO REGISTRO'));
        return $row[0];
    }

    /** Lista de tabla foránea 'ente'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'ente'.
    * @return result Resultado de la consulta de la de tabla foránea 'ente'.*/ 
    public function enteList() { return DbFunc::exeQryFile(self::_path().'ente_list_select.sql', $_POST); }

    /** Lista de tabla foránea 'dependencia'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'dependencia'.
    * @return result Resultado de la consulta de la de tabla foránea 'dependencia'.*/ 
    public function dependenciaList() { return DbFunc::exeQryFile(self::_path().'dependencia_list_select.sql', $_POST); }

    /** Lista de tabla foránea 'cargo'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'cargo'.
    * @return result Resultado de la consulta de la de tabla foránea 'cargo'.*/ 
    public function cargoList() { return DbFunc::exeQryFile(self::_path().'cargo_list_select.sql', $_POST); }

}

