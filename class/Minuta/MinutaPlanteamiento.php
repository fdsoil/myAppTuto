<?php
namespace myApp\Minuta;

use \FDSoil\Func as Func;
use \FDSoil\DbFunc as DbFunc;

/** MinutaPlanteamiento: Clase para actualizar y consultar la tabla 'minuta_planteamiento'.
* Utiliza '\FDSoil\Func' y '\FDSoil\DbFunc' respectivamente.*/
class MinutaPlanteamiento
{

    /** Devuelve la ruta en que están ubicados los archivos .sql de la clase MinutaPlanteamiento.
    * Descripción: Devuelve la ruta en que están ubicados los .sql (querys) de la clase MinutaPlanteamiento.
    * @return string La ruta en que están ubicados los archivos .sql (querys) de la clase MinutaPlanteamiento.*/
    private function _path() { return '../../../'.$_SESSION['myApp'].'/class/Minuta/sql/minuta_planteamiento/'; }

    /** Actualiza registro de la tabla 'minuta_planteamiento'.
    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla 'minuta_planteamiento'.
    * Nota: Requiere de los respectivos valores procedentes del $_POST para actualizar el registro.*/
    public function minutaPlanteamientoRegister()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'minuta_planteamiento_register_pl.sql',$_POST, true, 'ACTUALIZANDO REGISTRO'));
        return $row[0];
    }

    /** Obtener registro(s) de la tabla 'minuta_planteamiento'.
    * Descripción: Obtener registro(s) de la tabla 'minuta_planteamiento'.
    * Nota: Requiere el correspondiente valor $_POST identificativo del registro específico a consultar.
    * @return result Resultado con registro(s) de la tabla 'minuta_planteamiento'.*/
    public function minutaPlanteamientoGet()
    {
        return DbFunc::fetchAllAssoc(DbFunc::exeQryFile(self::_path().'minuta_planteamiento_get_select.sql', $_POST));
    }

    /** Elimina registro de la tabla 'minuta_planteamiento'.
    * Descripción: Realiza eliminación del registro ( delete ) de la tabla 'minuta_planteamiento'.
    * Nota: Requiere el valor $_POST identificativo para buscar y eliminar registro en base de datos.*/ 
    public function minutaPlanteamientoDelete()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'minuta_planteamiento_delete_pl.sql', $_POST, true, 'ELIMINANDO REGISTRO'));
        return $row[0];
    }

}

