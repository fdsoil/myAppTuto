-- Function: minuta.minuta_register(integer, integer, date, time without time zone, character varying, integer, integer, character varying, text, text)

-- DROP FUNCTION minuta.minuta_register(integer, integer, date, time without time zone, character varying, integer, integer, character varying, text, text);

CREATE OR REPLACE FUNCTION minuta.minuta_register(i_id integer, i_id_ciudad integer, i_fecha date, i_hora time without time zone, i_lugar character varying, i_id_ente integer, i_id_dependencia integer, i_asunto character varying, i_motivo text, i_observacion text)
  RETURNS json AS
$BODY$
DECLARE
        v_existe boolean;
        v_id integer;
        o_return json;
BEGIN
        o_return:=array_to_json(array['']);
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM minuta.minuta
                        WHERE id_ciudad=i_id_ciudad
                                        AND fecha=i_fecha
                                        AND hora=i_hora
                                        AND asunto=i_asunto;
                IF  v_existe='f' THEN
                        INSERT INTO minuta.minuta(
                                id_ciudad,
                                fecha,
                                hora,
                                lugar,
                                id_ente,
                                id_dependencia,
                                asunto,
                                motivo,
                                observacion)
                        VALUES (
                                i_id_ciudad,
                                i_fecha,
                                i_hora,
                                i_lugar,
                                i_id_ente,
                                i_id_dependencia,
                                i_asunto,
                                i_motivo,
                                i_observacion);
                        SELECT max(id) INTO v_id FROM minuta.minuta
                                WHERE id_ciudad=i_id_ciudad
                                        AND fecha=i_fecha
                                        AND hora=i_hora
                                        AND asunto=i_asunto;
                        o_return:= array_to_json(array['C', v_id::character varying]);
                ELSE
                        o_return:= array_to_json(array['T']);
                END IF;
        ELSE
                UPDATE minuta.minuta SET
                        lugar=i_lugar,
                        id_ente=i_id_ente,
                        id_dependencia=i_id_dependencia,
                        motivo=i_motivo,
                        observacion=i_observacion
                WHERE id=i_id;
                        o_return:= array_to_json(array['A']);
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION minuta.minuta_register(integer, integer, date, time without time zone, character varying, integer, integer, character varying, text, text)
  OWNER TO postgres;

-- Function: minuta.minuta_delete(integer)

-- DROP FUNCTION minuta.minuta_delete(integer);

CREATE OR REPLACE FUNCTION minuta.minuta_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM minuta.minuta WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION minuta.minuta_delete(integer)
  OWNER TO postgres;

-- Function: minuta.minuta_acuerdo_register(integer, integer, text, character varying, text)

-- DROP FUNCTION minuta.minuta_acuerdo_register(integer, integer, text, character varying, text);

CREATE OR REPLACE FUNCTION minuta.minuta_acuerdo_register(i_id integer, i_id_minuta integer, i_acuerdo text, i_responsable character varying, i_observacion text)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO minuta.minuta_acuerdo(
                        id_minuta,
                        acuerdo,
                        responsable,
                        observacion)
                VALUES (
                        i_id_minuta,
                        i_acuerdo,
                        i_responsable,
                        i_observacion);
                o_return:= 'C';
        ELSE
                UPDATE minuta.minuta_acuerdo SET
                        id_minuta=i_id_minuta,
                        acuerdo=i_acuerdo,
                        responsable=i_responsable,
                        observacion=i_observacion
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION minuta.minuta_acuerdo_register(integer, integer, text, character varying, text)
  OWNER TO postgres;

-- Function: minuta.minuta_acuerdo_delete(integer)

-- DROP FUNCTION minuta.minuta_acuerdo_delete(integer);

CREATE OR REPLACE FUNCTION minuta.minuta_acuerdo_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM minuta.minuta_acuerdo WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION minuta.minuta_acuerdo_delete(integer)
  OWNER TO postgres;

-- Function: minuta.minuta_asistente_register(integer, integer, character varying, character varying, integer, integer, integer, character varying, character varying, text)

-- DROP FUNCTION minuta.minuta_asistente_register(integer, integer, character varying, character varying, integer, integer, integer, character varying, character varying, text);

CREATE OR REPLACE FUNCTION minuta.minuta_asistente_register(i_id integer, i_id_minuta integer, i_cedula character varying, i_nombre character varying, i_id_ente integer, i_id_dependencia integer, i_id_cargo integer, i_correo character varying, i_telefono character varying, i_observacion text)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO minuta.minuta_asistente(
                        id_minuta,
                        cedula,
                        nombre,
                        id_ente,
                        id_dependencia,
                        id_cargo,
                        correo,
                        telefono,
                        observacion)
                VALUES (
                        i_id_minuta,
                        i_cedula,
                        i_nombre,
                        i_id_ente,
                        i_id_dependencia,
                        i_id_cargo,
                        i_correo,
                        i_telefono,
                        i_observacion);
                o_return:= 'C';
        ELSE
                UPDATE minuta.minuta_asistente SET
                        id_minuta=i_id_minuta,
                        cedula=i_cedula,
                        nombre=i_nombre,
                        id_ente=i_id_ente,
                        id_dependencia=i_id_dependencia,
                        id_cargo=i_id_cargo,
                        correo=i_correo,
                        telefono=i_telefono,
                        observacion=i_observacion
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION minuta.minuta_asistente_register(integer, integer, character varying, character varying, integer, integer, integer, character varying, character varying, text)
  OWNER TO postgres;

-- Function: minuta.minuta_asistente_delete(integer)

-- DROP FUNCTION minuta.minuta_asistente_delete(integer);

CREATE OR REPLACE FUNCTION minuta.minuta_asistente_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM minuta.minuta_asistente WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION minuta.minuta_asistente_delete(integer)
  OWNER TO postgres;

-- Function: minuta.minuta_planteamiento_register(integer, integer, text, character varying, text)

-- DROP FUNCTION minuta.minuta_planteamiento_register(integer, integer, text, character varying, text);

CREATE OR REPLACE FUNCTION minuta.minuta_planteamiento_register(i_id integer, i_id_minuta integer, i_planteamiento text, i_ponente character varying, i_observacion text)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO minuta.minuta_planteamiento(
                        id_minuta,
                        planteamiento,
                        ponente,
                        observacion)
                VALUES (
                        i_id_minuta,
                        i_planteamiento,
                        i_ponente,
                        i_observacion);
                o_return:= 'C';
        ELSE
                UPDATE minuta.minuta_planteamiento SET
                        id_minuta=i_id_minuta,
                        planteamiento=i_planteamiento,
                        ponente=i_ponente,
                        observacion=i_observacion
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION minuta.minuta_planteamiento_register(integer, integer, text, character varying, text)
  OWNER TO postgres;

-- Function: minuta.minuta_planteamiento_delete(integer)

-- DROP FUNCTION minuta.minuta_planteamiento_delete(integer);

CREATE OR REPLACE FUNCTION minuta.minuta_planteamiento_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM minuta.minuta_planteamiento WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION minuta.minuta_planteamiento_delete(integer)
  OWNER TO postgres;

