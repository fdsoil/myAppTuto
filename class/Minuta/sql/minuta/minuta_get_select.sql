SELECT 
    A.id,
    A.id_ciudad,
    A.fecha,
    A.hora,
    A.lugar,
    A.id_ente,
    A.id_dependencia,
    A.asunto,
    A.motivo,
    A.observacion
    ,B.descripcion AS des_ciudad
    ,C.descripcion AS des_dependencia
    ,D.descripcion AS des_ente
FROM minuta.minuta A
INNER JOIN minuta.ciudad B ON A.id_ciudad=B.id
INNER JOIN minuta.dependencia C ON A.id_dependencia=C.id
INNER JOIN minuta.ente D ON A.id_ente=D.id
{fld:where}
ORDER BY 2;
