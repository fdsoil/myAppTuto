SELECT 
    A.id,
    A.id_minuta,
    A.cedula,
    A.nombre,
    A.id_ente,
    A.id_dependencia,
    A.id_cargo,
    A.correo,
    A.telefono,
    A.observacion
    ,B.descripcion AS des_ente
    ,C.descripcion AS des_dependencia
    ,D.descripcion AS des_cargo
FROM minuta.minuta_asistente A
INNER JOIN minuta.ente B ON A.id_ente=B.id
INNER JOIN minuta.dependencia C ON A.id_dependencia=C.id
INNER JOIN minuta.cargo D ON A.id_cargo=D.id
WHERE A.id_minuta = {fld:id}
ORDER BY 3;
