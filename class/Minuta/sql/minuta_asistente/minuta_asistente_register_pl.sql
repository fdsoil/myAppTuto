SELECT minuta.minuta_asistente_register(
    {fld:id_minuta_asistente},
    {fld:id_minuta},
    '{fld:cedula}',
    '{fld:nombre}',
    {fld:id_ente},
    {fld:id_dependencia},
    {fld:id_cargo},
    '{fld:correo}',
    '{fld:telefono}',
    '{fld:observacion}'
);