SELECT 
    id,
    id_minuta,
    acuerdo,
    responsable,
    observacion
FROM minuta.minuta_acuerdo 
WHERE id_minuta = {fld:id}
ORDER BY 3;
