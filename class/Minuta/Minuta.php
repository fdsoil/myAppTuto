<?php
namespace myApp;

use \FDSoil\Func as Func;
use \FDSoil\DbFunc as DbFunc;

/** Minuta: Clase para actualizar y consultar la tabla 'minuta'.
* Utiliza '\FDSoil\Func' y '\FDSoil\DbFunc' respectivamente.*/
class Minuta
{

    /** Arreglo asociativo que contiene los correspondientes nombres de
    * campos y sus características respectivas. Dichos campos son los
    * únicos valores permitidos por la clase Minuta a traves de la
    * variable $_POST, para actualizar la tabla 'minuta'.*/
    static private $_aValReqs = [
        "id" => [
            "label" => "Id",
            "required" => false
        ],
        "id_ciudad" => [
            "label" => "Id Ciudad",
            "required" => false
        ],
        "fecha" => [
            "label" => "Fecha",
            "required" => false
        ],
        "hora" => [
            "label" => "Hora",
            "required" => false
        ],
        "lugar" => [
            "label" => "Lugar",
            "required" => false
        ],
        "id_ente" => [
            "label" => "Id Ente",
            "required" => false
        ],
        "id_dependencia" => [
            "label" => "Id Dependencia",
            "required" => false
        ],
        "asunto" => [
            "label" => "Asunto",
            "required" => false
        ],
        "motivo" => [
            "label" => "Motivo",
            "required" => false
        ],
        "observacion" => [
            "label" => "Observacion",
            "required" => false
        ]
    ];

    /** Devuelve la ruta en que están ubicados los archivos .sql de la clase Minuta.
    * Descripción: Devuelve la ruta en que están ubicados los .sql (querys) de la clase Minuta.
    * @return string La ruta en que están ubicados los archivos .sql (querys) de la clase Minuta.*/
    private function _path() { return '../../../'.$_SESSION['myApp'].'/class/Minuta/sql/minuta/'; }

    /** Obtener registro(s) de la tabla 'minuta'.
    * Descripción: Obtener registro(s) de la tabla 'minuta'.
    * Si el parámetro $label trae el argumento 'LIST', devuelve todos los registros de la tabla 'minuta'.
    * De lo contrario, si el parámetro $label trae el argumento 'REGIST', devuelve un solo registro,
    * siempre y cuando el valor de $_POST['id'] coincida con el ID principal de algún registro asociado.
    * Nota: Requiere el correspondiente valor $_POST['id'] del registro específico a consultar.
    * @param string $label Con sólo dos (2) posibles valores ('LIST' o 'REGIST').
    * @return result Resultado con registro(s) de la tabla 'minuta'.*/
    public function minutaGet($label)
    {
        switch ($label) {
            case 'LIST':
                $arr['where'] = '';
                break;
            case 'REGIST':
                $arr['id'] = $_POST['id'];
                $arr['where'] = Func::replace_data($arr, ' WHERE A.id = {fld:id} ');
                break;
        }
        return DbFunc::exeQryFile(self::_path().'minuta_get_select.sql', $arr);
    }

    /** Actualiza registro de la tabla 'minuta'.
    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla 'minuta'.
    * Nota: Requiere de los respectivos valores procedentes del $_POST para actualizar el registro.*/
    public function minutaRegister()
    {
        $aMsjReqs = Func::valReqs($_POST, self::$_aValReqs);
        if (!$aMsjReqs){
            $_POST = Func::formatReqs($_POST, self::$_aValReqs);
            $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'minuta_register_pl.sql',$_POST, true, 'ACTUALIZANDO REGISTRO'));
            $msj = $row[0];
        } else {
            $_SESSION['messages'] = $aMsjReqs;
            $msj = 'N';
        }
        return $msj;
    }

    /** Elimina registro de la tabla 'minuta'.
    * Descripción: Realiza eliminación del registro ( delete ) de la tabla 'minuta'.
    * Nota: Requiere el valor $_POST['id'] para buscar y eliminar registro en base de datos.*/ 
    public function minutaDelete()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'minuta_delete_pl.sql', $_POST, true, 'ELIMINANDO REGISTRO'));
        if ($row[0]!='B')
            Func::adminMsj($row[0],1);
        else
            header("Location: ".$_SERVER['HTTP_REFERER']);
    }

    /** Lista de tabla foránea 'ciudad'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'ciudad'.
    * @return result Resultado de la consulta de la de tabla foránea 'ciudad'.*/ 
    public function ciudadList()
    {
        return DbFunc::exeQryFile(self::_path().'ciudad_list_select.sql', $_POST);
    }

    /** Lista de tabla foránea 'dependencia'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'dependencia'.
    * @return result Resultado de la consulta de la de tabla foránea 'dependencia'.*/ 
    public function dependenciaList()
    {
        return DbFunc::exeQryFile(self::_path().'dependencia_list_select.sql', $_POST);
    }

    /** Lista de tabla foránea 'ente'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'ente'.
    * @return result Resultado de la consulta de la de tabla foránea 'ente'.*/ 
    public function enteList()
    {
        return DbFunc::exeQryFile(self::_path().'ente_list_select.sql', $_POST);
    }

}

