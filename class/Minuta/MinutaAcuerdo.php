<?php
namespace myApp\Minuta;

use \FDSoil\Func as Func;
use \FDSoil\DbFunc as DbFunc;

/** MinutaAcuerdo: Clase para actualizar y consultar la tabla 'minuta_acuerdo'.
* Utiliza '\FDSoil\Func' y '\FDSoil\DbFunc' respectivamente.*/
class MinutaAcuerdo
{

    /** Devuelve la ruta en que están ubicados los archivos .sql de la clase MinutaAcuerdo.
    * Descripción: Devuelve la ruta en que están ubicados los .sql (querys) de la clase MinutaAcuerdo.
    * @return string La ruta en que están ubicados los archivos .sql (querys) de la clase MinutaAcuerdo.*/
    private function _path() { return '../../../'.$_SESSION['myApp'].'/class/Minuta/sql/minuta_acuerdo/'; }

    /** Actualiza registro de la tabla 'minuta_acuerdo'.
    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla 'minuta_acuerdo'.
    * Nota: Requiere de los respectivos valores procedentes del $_POST para actualizar el registro.*/
    public function minutaAcuerdoRegister()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'minuta_acuerdo_register_pl.sql',$_POST, true, 'ACTUALIZANDO REGISTRO'));
        return $row[0];
    }

    /** Obtener registro(s) de la tabla 'minuta_acuerdo'.
    * Descripción: Obtener registro(s) de la tabla 'minuta_acuerdo'.
    * Nota: Requiere el correspondiente valor $_POST identificativo del registro específico a consultar.
    * @return result Resultado con registro(s) de la tabla 'minuta_acuerdo'.*/
    public function minutaAcuerdoGet()
    {
        return DbFunc::fetchAllAssoc(DbFunc::exeQryFile(self::_path().'minuta_acuerdo_get_select.sql', $_POST));
    }

    /** Elimina registro de la tabla 'minuta_acuerdo'.
    * Descripción: Realiza eliminación del registro ( delete ) de la tabla 'minuta_acuerdo'.
    * Nota: Requiere el valor $_POST identificativo para buscar y eliminar registro en base de datos.*/ 
    public function minutaAcuerdoDelete()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'minuta_acuerdo_delete_pl.sql', $_POST, true, 'ELIMINANDO REGISTRO'));
        return $row[0];
    }

}

