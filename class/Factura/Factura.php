<?php
namespace myApp;

use \FDSoil\Func as Func;
use \FDSoil\DbFunc as DbFunc;

class Factura
{

    private function _path() { return "../../../".$_SESSION['myApp']."/class/Factura/sql/factura/"; }
    
    function facturaGet($label)
    {        
        switch ($label){
            case 'LIST':
                $arr['where'] = '';
                break;
            case 'REGIST':
                $arr['id'] = $_POST['id'];
                $arr['where'] = Func::replace_data($arr, ' WHERE id = {fld:id} ');
	        break;			
        }        
        return DbFunc::exeQryFile(self::_path().'factura_get_select.sql', $arr);
    }

    function facturaAuxGet() { return DbFunc::exeQryFile(self::_path()."factura_aux_get_select.sql", $_POST); }

    function listProducto() { return DbFunc::exeQryFile(self::_path()."producto_get_select.sql", null); }

    function facturaRegist()
    {
        $sql=DbFunc::fetchRow(DbFunc::exeQryFile(self::_path()."factura_regist_pl.sql", $_POST));
        return $sql[0];
    }

}

